## **Task manager**
#### Описание
Данный учебный проект представляет собой систему управления проектами и задачами, реализующую применение CRUD операций.    
#### Технологии
Проект создан с помощью следующих технологий:
 + Java™ SE Development Kit 8

Среда разработки: 
 + IntelliJ IDEA 2018.3.1 (Community Edition)
#### Сборка приложения
Сборка приложения осуществляется с помощью [Apache Maven](https://maven.apache.org/index.html) 3.6.0(версия на момент разработки).
   
#### Запуск приложения
Для запуска приложения необходима установленная [Java](https://www.java.com) (версия 8 Update 192 или выше).   
В командной строке введите следующую команду:

    java -jar "полный путь к собранному jar файлу"
 
#### Дополнительно
Данное приложение тестировалось только на платформе Windows 10 10.0 x64
#### Разработчик
Морозов Андрей 

e-mail: aamorozov.mail@yandex.ru 

