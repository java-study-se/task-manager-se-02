package com.morozov.tm.repository;

import java.util.List;

public interface Repository<T> {

    List<T> readAll();

    T read(String name);

    void write(String name);

    void update(String oldName, String newName);

    void delete(String name);

}
