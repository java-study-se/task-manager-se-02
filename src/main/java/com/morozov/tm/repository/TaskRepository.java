package com.morozov.tm.repository;

import com.morozov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements Repository<Task> {
    private List<Task> taskList = new ArrayList<>();

    @Override
    public List<Task> readAll() {
        return taskList;
    }

    @Override
    public Task read(String name) {
        Task task = null;
        if (name != null || !name.isEmpty()) {
            for (Task t : taskList) {
                if (t.getTaskName().equals(name)) task = t;
            }
        }
        return task;
    }

    @Override
    public void write(String name) {
        taskList.add(new Task(name));
    }

    @Override
    public void update(String oldName, String newName) {
        Task task = read(oldName);
        if (task != null) task.setTaskName(newName);
    }

    @Override
    public void delete(String name) {
        Task task = read(name);
        if (task != null) taskList.remove(task);
    }

}
