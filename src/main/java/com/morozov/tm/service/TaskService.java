package com.morozov.tm.service;

import com.morozov.tm.entity.Task;
import com.morozov.tm.repository.TaskRepository;


public class TaskService {
    private static TaskRepository taskRepository = new TaskRepository();

    public static void showAllTask() {
        if (!taskRepository.readAll().isEmpty()) {
            ConsoleHelper.writeString("Список задач:");
            for (Task t : taskRepository.readAll()) {
                ConsoleHelper.writeString(t.getTaskName());
            }
        } else {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }

    public static void writeTask(String name) {
        if (!name.isEmpty()) taskRepository.write(name);
        else ConsoleHelper.writeString("Имя не введено");
    }

    public static void deleteTask(String name) {
        if (!name.isEmpty()) taskRepository.delete(name);
        else ConsoleHelper.writeString("Имя не введено");
    }

    public static void updateTask(String oldName, String newName) {
        if (!oldName.isEmpty() && !newName.isEmpty()) taskRepository.update(oldName, newName);
        else ConsoleHelper.writeString("Имя не введено");
    }
}
